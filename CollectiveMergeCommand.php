<?php

declare(strict_types=1);

namespace Oxyshop\Ned\Command\Gitlab;

use Gitlab\Api\MergeRequests;
use Gitlab\Client;
use Gitlab\Model\Project;
use Oxyshop\Ned\Command\Helper\GitlabApiHelper;
use Oxyshop\Ned\Factory\GitlabApiFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CollectiveMergeCommand extends Command
{
    /**
     * @var GitlabApiHelper
     */
    protected $gitlabApiHelper;

    /**
     * @var Client
     */
    protected $gitlabClient;

    /**
     * @var MergeRequests
     */
    protected $mergeRequests;
    /**
     * @var SymfonyStyle
     */
    private $consoleOutput;

    /**
     * @var array
     */
    private $notMergedProjects = [];

    /**
     * CollectiveMergeCommand constructor.
     *
     * @param GitlabApiHelper  $gitlabApiHelper
     * @param GitlabApiFactory $factory
     * @param string|null      $name
     */
    public function __construct(GitlabApiHelper $gitlabApiHelper, GitlabApiFactory $factory, ?string $name = null)
    {
        parent::__construct($name);

        $this->gitlabApiHelper = $gitlabApiHelper;
        $this->gitlabClient = $factory->createClient();
        $this->mergeRequests = $this->gitlabClient->mergeRequests();
    }

    protected function configure()
    {
        $this
            ->setName('gitlab:collective:merge')
            ->addArgument('namespace', InputArgument::REQUIRED, 'Namespace of project destination you wish to be merged, e.g. oxid for shops.')
            ->addArgument('sourceBranch', InputArgument::REQUIRED, 'Source branch you wish to be merged in, e.g. DEVENM-1840')
            ->setDescription('Merges one branch to multiple repositories. Repositories not having request with given source branch are ignored.');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->consoleOutput = new SymfonyStyle($input, $output);

        $namespace = $input->getArgument('namespace');
        $sourceBranch = (string) $input->getArgument('sourceBranch');
        $projects = $this->getProjects($namespace);
        $this->mergeAllProjects($projects, $sourceBranch);

        if (empty($this->notMergedProjects)) {
            return;
        }

        $this->consoleOutput->warning("Following projects don't have exactly one merge request with source branch $sourceBranch and were skipped:");
        foreach ($this->notMergedProjects as $notMergedProject) {
            $this->consoleOutput->writeln("<error>$notMergedProject</error>");
        }
    }

    /**
     * @param string $namespace Namespace in which git project are stored
     *
     * @return array
     */
    protected function getProjects(string $namespace): array
    {
        return $this->gitlabApiHelper->getProjectsForGroup($namespace);
    }

    /**
     * @param array  $projects     Projects from given namespace
     * @param string $sourceBranch Source branch to be merged in
     */
    protected function mergeAllProjects(array $projects, string $sourceBranch): void
    {
        foreach ($projects as $project) {
            $mergeRequest = $this->getMergeRequests($project, $sourceBranch);
            $mergeRequestIid = $this->getMergeRequestIid($mergeRequest, $project->name);
            $destinationBranch = $this->getMergeTarget($mergeRequest);
            $this->doMerge($project, $mergeRequestIid, $sourceBranch, $destinationBranch);
        }
    }

    /**
     * @param Project $project      Single project from which merge requests will be selected
     * @param string  $sourceBranch Name of source branch used for filtering merge requests
     *
     * @return array
     */
    protected function getMergeRequests(Project $project, string $sourceBranch): array
    {
        $requestOptions = ['state' => 'opened', 'source_branch' => $sourceBranch];

        return $this->mergeRequests->all($project->id, $requestOptions);
    }

    /**
     * @param array  $requests    Array of merge requests
     * @param string $projectName Name of project
     *
     * @return int|null
     *
     * While it shouldn't be possible to have multiple branches we check for existence of exactly one just to be sure.
     * Could return null if given project doesn't have any merge request with given source branch.
     */
    protected function getMergeRequestIid(array $requests, string $projectName): ?int
    {
        $requestIid = array_column($requests, 'iid');

        if (count($requestIid) !== 1) {
            $this->notMergedProjects[] = $projectName;

            return null;
        }

        return $requestIid[0];
    }

    /**
     * @param array $requests Array of merge Requests
     *
     * @return string|null
     *
     * Could return null if given project doesn't have any merge request with given source branch
     */
    protected function getMergeTarget(array $requests): ?string
    {
        $target = array_column($requests, 'target_branch');

        return $target[0] ?? null;
    }

    /**
     * @param Project     $project         Single project in which source branch will be merged in
     * @param int|null    $mergeRequestIid Iid of merge request
     * @param string      $sourceBranch    Name of source branch
     * @param string|null $targetBranch    Name of target branch
     */
    protected function doMerge(Project $project, ?int $mergeRequestIid, string $sourceBranch, ?string $targetBranch): void
    {
        if (null === $mergeRequestIid) {
            return;
        }

        $message = "Merging $sourceBranch into $targetBranch";
        $this->consoleOutput->writeln("<info>$message at $project->name</info>");

        $response = $this->mergeRequests->merge($project->id, $mergeRequestIid, $message);

        if ( ! $response) {
            $this->consoleOutput->writeln("<error>Merging $sourceBranch at $project->name failed!</error>");
        }
    }
}
