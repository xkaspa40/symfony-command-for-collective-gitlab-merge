# Symfony command for collective gitlab merge
Ever been in a situation when you had a LOT of merge requests (of the same commit which was probably cherrypicked) into many different repositories? Yeah, it's no fun clicking like a monkey... so I programmed a command for symfony which enables you to merge all those requests by providing a name of given merge request(s).
